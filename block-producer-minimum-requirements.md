# Libre Blockchain Block Producer Minimum Requirements:
 
The intent of this document is to clarify the minimum requirements for Block Producers to participate in the Libre Blockchain. This document is an addendum to the Libre Blockchain Operating Agreement and Regproducer Agreement and inherits their terms and language. The specifications stated herein are subject to revision by the Libre Block Producers as determined by a multi-signature action of 2/3+1 of the current Block Producersat any time.  Regardless of their level of participation, to participate in the Libra Blockchain, Block Producer candidates are required to provide, and abide by the following:

### A. Disclosures:

#### 1.  Block Producer Account Name

#### 2.  Block Producer Block-Signing Public Key

#### 3.  Block Producer Organization Info:

a. Candidate Name
          
b. Candidate Website URL
          
c. Candidate country of registration for registered entity or residence of primary owner if not a registered entity as 2-letter ISO country coded. 

d. Candidate server location(s)
         
i. Location name
                      
ii. Country as 2-letter country code
                      
iii. latitude
                      
iv. Longitude

v. geographically-based numeric code in compliance with the block producer schedule optimizing structure implemented in the Libre eosio system contract.
                      
#### 4: Network Emergency Contact(s)

For each owner and lead system administrator (if different): 

a. Full name
          
b. email address
          
c. mobile phone number - in a non-public, password protected, repository commonly accessible to any of the paid Block Producers and Standby Block Producers.

### B. Practices:

1. Sync with a mutually approved network time protocol (NPT) server at least once per 24 hours.

## Minimum Requirements

#### 1. Endpoints: 

a. Mainnet P2P endpoint 
          
b. Mainnet HTTP API endpoint
          
c. Mainnet HTTPS API endpoint

#### 2: Nodes: 

a. Testnet Node

i. Minimum RAM (per node): 8GB
          
ii. Minimum Disk (per node): 100GB

iii. A minimum of 1,200,000 blocks (approximately 7 days) of synched and registered operation of a Libre testnet block production node measured from the first block produced.

iv. Ongoing operation of a synched, registered, and publicly accessible Libre testnet block production node within 500,000 blocks (approximately 70 hours).

v. A maximum total of 1,000,000 blocks of Libre testnet non-operational time within the most recent 5,000,000 blocks (approximately 29 days). Enforcement of which commences at Libre mainnet block height 5,000,000. Failure to serve the prescribed amount of time on the Libre testnet will result in a votebpout multi-signature action to remove the Block Producer from service for 168 hours (7 days).

b. Producing Node

i. Minimum RAM (per node): 16GB
          
ii. Minimum Disk (per node): 100GB
          
iii. Firewall: Active
            
iv. Plugins: chain, producer

v. The Over-clocking setting shall not be used on block producer nodes.
      
c. Full Node 

i. Minimum RAM (per node): 16GB
          
ii. Minimum Disk (per node): 100GB
          
iii. Firewall: Active
            
iv. Plugins: chain, chain_api

# Copyright:

This document is in the public domain.
