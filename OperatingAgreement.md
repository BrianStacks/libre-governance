# Libre Blockchain Operating Agreement (LBOA)

This Agreement is a multi-party contract entered into by the Members by virtue of their use of this blockchain.

## 1. Preamble

This document is intended to set common agreements among all users of the Libre Blockchain in order to establish a secure, stable, and governable blockchain where value and information can be stored and all disputes shall be arbitrated solely using the methods described herein and within the “Libre Governance Documents” comprised of this Agreement, the “Libre Block Producer Minimum Requirements” and the Libre “regproducer” contract human language terms. Each document in the Libre Governance Documents is hereby incorporated herein by reference as if fully set forth herein. The Libre Blockchain is comprised of Members (as defined below) who have chosen to organize themselves using a computer blockchain as a form of transnational exchange of value, information, and commerce. These Members have agreed to govern the Libre Blockchain, its transactions and native token by means of this Libre Blockchain Operating Agreement (this “Agreement”) which enumerates a set of mutual representations and agreements amongst its users. 

## 2. Member Definition and Responsibilities

Any person or entity who uses the Libre Blockchain Blockchain to store, exchange, or process value or information shall be herein referred to as a “Member.” Each Member agrees to be bound exclusively by the terms of this Agreement or the most current Libre Governance Documents ratified or amended in accordance with the terms herein, and to select the terms of the current Agreement at the time of the transaction as binding in the interactions between them regarding any and all value and information stored on the Libre Blockchain. It is the responsibility of each Member to abide by their local terrestrial laws and statutes.

## 3. Recording of Values and Information

The Libre Blockchain shall be used to record value and information and to perform computations pertaining to these. This information shall be recorded in a continuous chain of blocks of transactions and cryptographic security information (“Blocks”) which are tested and validated by computer nodes on the network (“Validating Nodes”).

## 4. Accounts

A Member may control one or more accounts on the blockchain. Accounts record the ownership of various tokens and resources and are controlled by cryptographic keys. Accounts may publish smart contracts including contracts that issue new tokens.

## 5. Native Unit of Value

The native unit of value on the Libre Blockchain shall be the LIBRE token. Each LIBRE token shall represent a percentage of the usage of the Libre Blockchain shared resources in direct relation to the total number of LIBRE tokens that are recorded on the blockchain. As a pro rata percentage of shared resource usage rights, each LIBRE token shall entitle the account holding it to proportional voting rights and use of the Libre Blockchain’s operational computer resources.

## 6. Operation and Execution of the Blockchain

The Libre Blockchain reaches consensus regarding the values and transactions it records using delegated proof of stake, whereby active Validating Nodes (“Block Producers”) and standby Validating Nodes (“Standby Block Producers”) are elected by the votes of the LIBRE token holders to serve as delegates in the processing and validating of transactions and blocks in the blockchain according the technical specifications of the Libre branch of the Antelope blockchain protocol software.

## 7. Modifying the Blockchain

Transactions on the Libre Blockchain shall be reversible only until the Block Producers reach final consensus regarding the validity of the block containing them. Once more than two-thirds of the Block Producers have proposed a block to be valid and an additional Block Producer has accepted the proposed valid block, then it shall be deemed an “Irrevocable Block.” The Libre Blockchain shall never amend any Irrevocable Block. Modifications to the blockchain may only be made through rejecting blocks not yet accepted as Irrevocable Blocks and through appending new information into current blocks.

## 8. Block Producer Nomination, Qualification, and Election

Any Libre Member may self-nominate as a candidate to be a Block Producer by executing the "regproducer" action of the "eosio" contract and accepting its human-language terms as a binding contract. The Libre Blockchain maintains a list of minimum requirements for acting as a Block Producer or Standby Block Producers (the “Block Producer Minimum Requirements”). The 30 block producer candidates currently in compliance with the Block Producer Minimum Requirements receiving the highest weight of Member votes shall serve as Block Producers. The 6 block producer candidates currently in compliance with the Block Producer Minimum Requirements and not serving as Block Producers receiving the highest weight of Member votes shall serve as Standby Block Producers. Enforcement of compliance with the Block Producer Minimum Requirements shall be by a 2/3+1 vote of the Block Producers.

## 9. Block Producers Responsibilities

Block Producers shall operate computer infrastructure in accordance with the Libre Block Producer Minimum Requirements. Block Producers are to produce blocks at their scheduled time in accordance with the human-language terms of the "regproducer" action of the "eosio" contract. As the executive authority of the Libre Blockchain, the Block Producers, in aggregate, have broad executive powers over the Libre Blockchain. They may pause the Libre Blockchain, implement new software updates, mint new tokens, implement additional operational rules for the blockchain that do not violate the Agreement, and adjudicate and/or assign cases for arbitration and enforce disputes at their sole discretion by voting for any such action with a majority vote of greater than two-thirds of the Block Producers at any given time.

## 10. Freezing Accounts

The Block Producers may issue an emergency order to freeze all transactions from a Member account or accounts as an interim measure which preserves the status quo and preserves assets while a claim is being investigated and decided. The Block Producers do not have the obligation to freeze account transactions. All decisions will be at the sole discretion of the set of Block Producers currently in operation based on the schedule produced by the then operating protocol software. The Block Producers may reassign or nullify the controlling cryptographic keys of the account or accounts.

## 11. Voting

Libre Members may vote to elect block producer candidates as described in this Agreement. Each Member may vote the entire number of LIBRE tokens in any account they control via private keys. Members may vote for up to 4 block producer candidates at any time and each vote for any candidate will share the same weight as that of all the other candidates. The Block Producers may enact global rules to modify the voting weight based on the age of the votes, the number of votes cast by an account or any other method they deem appropriate by deploying an update of the Libre software designed for this purpose.

## 12. Proxies
Members may delegate their votes to another Member as a voting proxy (“Proxy”) using the “proxy” contract included in the Antelope software. Only a token’s true beneficial owner or a voting Proxy recorded on the blockchain may vote tokens. No block producer or block producer candidate shall operate a Proxy either directly or by any owner or employee of the block producer or candidate. Any Member holding tokens in trust for another beneficial owner, such as a centralized exchange, may not operate a Proxy, cast votes for or assign to a Proxy such tokens. The Block Producers have the authority to remove the proxy power of any Proxy in violation of these rules.

## 13. Initial Token Allocation

The initial distribution of LIBRE tokens on the Libre Blockchain (“Libre Initial Distribution”) will be determined as follows. Prior to the creation of the Libre Blockchain, no LIBRE tokens shall exist and no promise of future tokens shall be made by any Member. Upon the creation of the Libre Blockchain (on or about July 4, 2022), Block Producers shall earn a block reward for each block they produce that is incorporated into the permanent blockchain record. Block rewards shall initially be 4 LIBRE tokens per block and that amount shall be reduced by half at exactly six months after the creation of the blockchain to 2 LIBRE tokens per block, and halved again exactly twelve months after the creation of the blockchain to 1 LIBRE per block. A total of ten million LIBRE tokens shall be reserved for free distribution (aka "Airdrop" or "Spindrop") to any claimant who can provide a cryptographic signiture proving ownership of any account with a balance equivalent to one United States of America dollar in that blockchain's system token on the EOS, Telos, WAX, Proton, Ethereum, or Solana blockchains as of the snapshot date (on or about April 14, 2022). Claimants will receive an amount of LIBRE tokens determined by a randomization algorithm for each account claimed and may direct it to any Libre Blockchain account they designate. This free distribution period shall end when the paid distribution (aka "Mint Rush") period begins (on or about December 15, 2022). All tokens reserved for the Airdrop that remain unclaimed at the end of the claiming period shall be transferred to the Libre DAO Fund account ("dao.libre") for future distribution under the rules of the contract deployed on that account by the Block Producers. The paid distribution will distribute a total of 200 million LIBRE tokens in exchange for P Networks "pegged Bitcoin" PBTC tokens over a thirty-day purchase period based on an algorithm that factors the product of a multiplier that declines at an even rate each day of the period plus a second multiplier that grows proportionately for each day the purchaser elects to stake their purchased LIBRE tokens from 30 to 365 days. Purchasers shall receive their LIBRE tokens as a pro rata distribution of the tokens allotted to the Mint Rush in the form of Liquidity Pool ("LP") tokens for the PBTC/LIBRE LP, designated BTCLIB tokens. These LP tokens shall be locked in a staking contract ("stake.libre") and a portion shall be unlocked for claiming as liquid BTCLIB tokens daily. The purchaser shall be able to withdraw the LIBRE tokens purchased along with all staking rewards accrued once the staking period is complete. Alternatively, the purchaser may elect to withdraw LIBRE tokens prior to the completion of their staking period by relinquishing all accrued interest and 20% of the principal LIBRE tokens purchased.

## 14. Libre DAO Fund

LIBRE tokens shall be collected in a "DAO Fund" account ("dao.libre") for distribution by the votes of Members using a voting power algorithm that computes the voting power of any account as the product of the number of LIBRE tokens staked and the number of days remaining in their staking period. The intended purpose of the DAO Fund is to provide a source of future funding for any needs the Libre Blockchain may have beyond block rewards, including, without limitation, pay for the development of core software code, user interfaces and applications, marketing and promotion, and memberships in industry organizations.

## 15. Libre DAO Tax

In order to continuously fund the DAO Fund account, one tenth of each staking LIBRE token transaction amount shall be deposited as newly minted LIBRE tokens into the DAO Fund account as a "DAO Tax".

## 16. Libre DAO Proposal Submission

Any Libre Member or group of Members may submit proposals for the usage LIRBE tokens accumulated in the DAO Fund account by executing the required actions to compose and submit a proposal to the "dao.libre" contract (the “Proposer”) and providing the required information including, at least, a full description of the proposal, a link to a fixed source of information, and the exact deposit transaction, including deposit account or accounts, that will occur should the proposal be accepted by the Libre Members.

## 17. Libre DAO Proposal Voting Period

Once a proposal is submitted, there will be a period of three million blocks (approximately 17 days) in which a proposal may be voted (the “Voting Period”).

## 18. Libre DAO Proposal Submission Fee

A non-refundable submission fee of 2,000.0000 LIBRE shall be required as part of each DAO Proposal submission. The Proposer may elect to include add the amount of the submission fee in the proposal to be paid if the propoal is accepted.

## 19. Passage of a Libre DAO Proposal
Voting on a DAO Proposal shall be tallied using the total number of LIBRE tokens staked by each account at the time of the closing of the Voting Period without any voting power algorithm employed. Any Libre DAO Proposal that receives a 51% majority of YES votes and a minimum threshold of 10.0% of total votes from all issued LIBRE tokens at the conclusion of the Voting Period shall be an “Accepted Proposal.”

## 20. Execution of a Libre DAO Proposal

Each Accepted Proposal shall have the transaction described in the proposal available to execute immediately by a smart contract action by its Proposer, provided sufficient funds exist in the DAO Fund account. 

## 21. Failure to Provide Libre DAO Proposal Deliverables
The Block Producers may, by a more than two-thirds majority vote, elect to reclaim some or all of the disbursed funds from an Accepted Proposal that has been executed, but where the proposed work appears not to have been performed as described in the Proposal. Any funds reclaimed shall be returned to the Libre DAO account.

## 22. No Fiduciary or Partnership

No Member shall have fiduciary responsibility to support the value of the LIBRE token. The Members do not authorize anyone to hold assets, borrow, nor contract on behalf of the Members collectively. The Libre Blockchain Network has no owners, managers nor fiduciaries. No Member intends or claims partnership with any other Member on the basis of this Agreement. Each Member is an individual person or entity and no general or limited partnership is created or joined by engaging with the Libre Blockchain.

## 23. Publication to the Libre Blockchain

Each Member grants all other Members an irrevocable license to view transactions as recorded and published to the blockchain with ownership of the exact text published on the chain considered to be in the public domain.

## 24. Responsibility for Information

Members are advised that all data posted to the Libre Blockchain is intended to be stored immutably for as long as computers may exist. The Libre Blockchain, its Block Producers, developers and Members shall bear no responsibility or enforcement role for any information, data, or content improperly recorded on the blockchain. Any penalties or Decisions for improperly recorded information, data, or content shall be the sole responsibility of the Member that posted it.

## 25. Restitution

Each Member agrees that penalties for breach of this contract may include, but are not limited to, fines, loss of account, and any other measures decided by Block Producers as defined in this Agreement. No Block Producer shall be deemed liable for restitution for any action performed as a Block Producer in compliance with of the pertinent terms of this Agreement in force at the time of the action.

## 26. Amending

The Libre Governance Documents may be amended by a greater than two-thirds majority vote of the Block Producers.

## 27. Severability

If any part of this Agreement is declared unenforceable or invalid, the remainder will continue to be valid and enforceable. No part of this Agreement is to be given higher importance than any other due to its ordinal position within the document. Paragraph titles are intended for reference purposes only and shall not be interpreted to modify the meaning of the text of the paragraph.

## 28. Termination

A Member is automatically released from all revocable obligations under this Agreement five years after the Member has sold or otherwise relinquished all LIBRE tokens. 

## 29. Grammar

Any noun in this document used in the singular form shall also apply in the plural form and vice versa. Likewise, any masculine, feminine or neutral reference shall apply to every gender or neutral tense.

## 30. Consideration

All rights and obligations under this Agreement are mutual and reciprocal and of equally significant value and cost to all parties.

## 31. Acceptance

A contract is deemed accepted when a Member signs any transaction on the Libre Blockchain which incorporates a Transaction as Proof of Stake (TAPOS) cryptographic proof of an action within a block whose implied state incorporates an Application Binary Interface (ABI) of said contract and said transaction is incorporated into the blockchain.

## 32. Counterparts
This Agreement may be executed in any number of counterparts, each of which when executed and delivered shall constitute a duplicate original, but all counterparts together shall constitute a single agreement.

## 33. Complete Agreement

This Agreement is accepted as complete and needs no further ratification to be valid and enforceable. 

